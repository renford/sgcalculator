class InvalidInputException(Exception):
    '''Indicates an invalid value passed to the calculator '''         
    pass