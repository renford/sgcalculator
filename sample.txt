
>>> from sgcalculator.src.calculator import Calculator

>>> sample_data = [('sara', 1921, 1953),('Jane', 1943, 1987),('Jose', 1903, 1954),('Josh', 1981, 2000),
              	('Oni', 1960, 1993),('Li', 1923, 1994),('Derick', 1910, 1977),('Joe', 1936, 1990),
              	('Jeseph', 1943, 1987),('Jay', 1912, 1966),('Xu', 1941, 1991),('Lin', 1901, 1921),
              	('Danny', 1900, 1977),('Sai', 1930, 1985),('Uri', 1913, 1967),('Eric', 1963, 1979),
              	('Tom', 1919, 1961),('Jake', 1908, 1992),('Rod', 1944, 1994),('ciara', 1973, 1996),
              	('Zen', 1937, 1974),('Zoe', 1908, 1909),('Emily', 1953, 1984),('Sage', 1950, 1983),
              	('Kay', 1993, 1999),('Lane', 1900, 2000),('Cary', 1920, 1980),('Chris', 1930, 1970),
              	('Landon', 1940, 1947),('Murr', 1925, 1955),]

>>> cal = Calculator()

>>> cal.getMax(sample_data)
[1944, 1945, 1946, 1950, 1951, 1952, 1953]

see test modules for more examples.