from setuptools import setup

setup(name='sgcalculator',
      version='0.14',
      description=' Simple analytics module for looking at population data.',
      url='https://bitbucket.org/renford/sgcalculator',
      author='Renford Alexander',
      author_email='alexander.Renford@gmail.com',
      license='MIT',
      packages=['sgcalculator', 'sgcalculator/src', 'sgcalculator/test'],
      zip_safe=False)
